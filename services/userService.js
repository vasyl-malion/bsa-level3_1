const { UserRepository } = require('../repositories/userRepository');

class UserService {

    getAllUsers() {
        const items = UserRepository.getAll();
        if (!items) {
            return null;
        }
        return items
    }

    searchUser(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }


    createUser(data) {
        const item = UserRepository.create(data);
        const filterResult = Object.entries(item)
            .filter( item =>
                item[1] ===
            // user.lastName ||
            // user.firstName ||
            // user.email ||
            // user.password ||
            item.phoneNumber);
    let filterObj = Object.fromEntries(filterResult);
        console.log(filterObj);
        if (!filterObj) {
            return null;
        }
        return filterObj
    }

    updateUser(id, data) {
        const updatedUser = UserRepository.update(id, data);
        if (!updatedUser) {
            return null;
        }
        return updatedUser;
    }

    deleteUser(id) {
        const deletedUser = UserRepository.delete(id);
        if (!deletedUser) {
            return null;
        }
        return deletedUser;
    }
}

module.exports = new UserService();